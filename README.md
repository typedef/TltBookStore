# TltBookStore

Deploy проекта - первичная инициализация:

* Скачиваем проект командой (заранее перейдите в каталог для разработки csharp based проектов)
>git clone https://codeberg.org/typedef/TltBookStore TltBookStore

* Запустить back-end, откройте
>TltBookStore/TltBookStore.sln
>Правой кнопкой мыши нажми на  и выбери rebuild solution ![Alt text](ReadmeData/sln-image.png)

* Запустить БД (run in terminal):
> cd TltBookStore/DB && mongod -dbpath "./"
Просматривать содержимое БД нужно через MongoDB Compass

* Запустить сайт (UI, SPA - single page application):
> cd TltBookStore/SPA && ng serve
