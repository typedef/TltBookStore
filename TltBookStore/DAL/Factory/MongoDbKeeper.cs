using DAL.Contracts;
using Types.Types;

namespace DAL.Factory;

public class ImageFileInfo
{
    public byte[] Image { get; set; }
    public string Path { get; set; }
    public GenreType Genre { get; set; }
}

public static class MongoDbKeeper
{
    private static MongoDbFactory _mongoDbFactory;
    
    public static void Init()
    {
        _mongoDbFactory = new MongoDbFactory();
        _mongoDbFactory.InitRepositories();
    }
    
    public static async Task Deploy(List<ImageFileInfo> imageInfos)
    {
        _mongoDbFactory = new MongoDbFactory();
        await _mongoDbFactory.RecreateRepositories(imageInfos);
    }

    public static T GetRepository<T>(string repositoryName)
    {
        return (T) _mongoDbFactory.GetRepository(repositoryName);
    }
}