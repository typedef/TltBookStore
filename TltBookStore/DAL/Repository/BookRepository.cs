using System.Text;
using DAL.Contracts;
using DAL.Factory;
using MongoDB.Bson;
using MongoDB.Driver;
using Types.Types;
using Types.ViewModels;

namespace DAL.Repository;

public class BookRepository : IBookRepository, IMongoRepository
{
    private readonly IMongoCollection<BookViewModel> _booksCollection;

    public BookRepository(IMongoCollection<BookViewModel> booksCollection)
    {
        _booksCollection = booksCollection;
    }

    private static Dictionary<string, BookViewModel> _progBooksByFileName = new Dictionary<string, BookViewModel>()
    {
        {
            "ComputerScienceMinimum", new BookViewModel()
            {
                Producer = ProducerType.Piter,
                Name = "Теоретический минимум по Computer Science. Все что нужно программисту и разработчику",
                Author = "ФИЛО ВЛАДСОН ФЕРРЕЙРА",
                AgeLimit = 12,
                PublishingDate = 2018,
                PagesCount = 224,
                Genre = GenreType.ComputerAndProgramming,
                Price = 979,
                Description =
                    @"Хватит тратить время на скучные академические фолианты! Изучение Computer Science может быть веселым и увлекательным занятием.
                Владстон Феррейра Фило знакомит нас с вычислительным мышлением, позволяющим решать любые сложные задачи. Научиться писать код просто - пара недель на курсах, и вы «программист», но чтобы стать профи, который будет востребован всегда и везде, нужны фундаментальные знания. Здесь вы найдете только самую важную информацию, которая необходима каждому разработчику и программисту каждый день.
                «Эта книга пригодится и для решения повседневных задач. Упреждающая выборка и кэширование помогут сложить рюкзак, параллелизм облегчит готовку на кухне. Ну и, разумеется, ваш программный код будет просто потрясающим.»"
            }
        },

        {
            "GameProgrammingPatterns", new BookViewModel()
            {
                Producer = ProducerType.Bombora,
                Name = "Паттерны программирования игр",
                Author = "НИСТРЕМ РОБЕРТ",
                AgeLimit = 12,
                PublishingDate = 2022,
                PagesCount = 432,
                Genre = GenreType.ComputerAndProgramming,
                Price = 1799,
                Description =
                    @"Хороший код — основа любой игры. Работа игрового программиста состоит из множества задач, среди которых оптимизация игры, повышение производительности и создание надежной архитектуры. Одна из наиболее частых проблем, с которыми сталкиваются программисты, — разрастание кода до неконтролируемых размеров, что нередко обескураживает команду и даже встает на пути выхода игры. Как с этим бороться? У автора книги, программиста Electronic Arts Роберта Нистрема, есть для вас парочка действенных советов.
Каждая глава книги — набор эффективных паттернов программирования, которые вы можете использовать в работе в готовом виде или улучшать и комбинировать их на свой лад для вашей собственной игры.
«Лучший сборник практических рецептов реализации типовых задач в играх. Компактно, понятно, без информационного шума. Равномерный срез огромного пласта решений, уже принятых в игровой индустрии. Эта книга — отличная начальная точка для углубленного изучения конкретных алгоритмов и реализаций». — Кирилл Шабордин, CTO, W4 Kit / Cubelaria",
            }
        },

        {
            "RistInAction", new BookViewModel()
            {
                Producer = ProducerType.Bombora,
                Name = "Rust в действии",
                Author = "МАКНАМАРА ТИМ",
                AgeLimit = 0,
                PublishingDate = 2023,
                PagesCount = 528,
                Genre = GenreType.ComputerAndProgramming,
                Price = 1599,
                Description =
                    @"Книга о прикладных аспектах языка программирования Rust, описывающая внутреннее устройство языка и сферы его использования. Rust рассматривается как современное дополнение для С при программировании ядра ОС и при системном программировании, а также как низкоуровневый скоростной язык, обеспечивающий максимальную производительность. Объяснены тонкости работы с процессором, многопоточное программирование, работа с памятью, а также взаимодействие с Linux. Изложенный материал позволяет как писать современные приложения на Rust с нуля, так и внедрять Rust в сложившуюся базу кода.
Книга ориентирована на специалистов по С, Linux, системному программированию и на всех, кто желает освоить Rust и сразу приступить к работе с ним."
            }
        },

        {
            "JavaProgramming", new BookViewModel()
            {
                Producer = ProducerType.Ecsmo,
                Name = "Программирование на Java для начинающих",
                Author = "ВАСИЛЬЕВ АЛЕКСЕЙ НИКОЛАЕВИЧ",
                AgeLimit = 12,
                PublishingDate = 2022,
                PagesCount = 704,
                Genre = GenreType.ComputerAndProgramming,
                Price = 1299,
                Description =
                    @"Полный спектр сведений о языке Java с примерами и разбором задач от автора учебников-бестселлеров по языкам программирования Алексея Васильева. С помощью этой книги освоить язык Java сможет каждый желающий - от новичка до специалиста.",
            }
        },

        {
            "RobotTechPractiveCppAndRasberryPi", new BookViewModel()
            {
                Producer = ProducerType.Ecsmo,
                Name = "Практическая робототехника. C++ и Raspberry Pi",
                Author = "БРОМБАХ ЛЛОЙД",
                AgeLimit = 0,
                PublishingDate = 2023,
                PagesCount = 448,
                Genre = GenreType.ComputerAndProgramming,
                Price = 1499,
                Description =
                    @"Рассказано о технологии создания автономных роботов на базе одноплатного компьютера Raspberry Pi и о разработке программ для них на языке С++. Показаны принципы написания и даны примеры кода для контроллера привода двигателя, продемонстрированы способы использования датчиков для обнаружения препятствий и построения карт на основе данных лидара. Описаны методы разработки собственных алгоритмов автономного планирования траектории движения, приведен код для автоматической отправки путевых точек контроллеру привода. Рассмотрены библиотеки С++ для написания программ картографии и навигации автономных роботов, даны сведения об использовании контактов аппаратного интерфейса Raspberry Pi GPIO. Электронный архив на сайте издательства содержит код описанных в книге программ."
            }
        },

        {
            "LinuxAndGo", new BookViewModel()
            {
                Producer = ProducerType.BhvSpb,
                Name = "Linux и Go. Эффективное низкоуровневое программирование",
                Author = "ЦИЛЮРИК ОЛЕГ ИВАНОВИЧ",
                AgeLimit = 0,
                PublishingDate = 2023,
                PagesCount = 304,
                Genre = GenreType.ComputerAndProgramming,
                Price = 1999,
                Description =
                    @"Первая русскоязычная книга об интеграции языка Go в архитектуру ядра Linux. Содержит вводный курс по языку Go для системного программирования, описывает применение Go для реализации конкурентности и параллелизма, рассказывает об интероперабельности между имеющимся кодом на C с новым кодом на Go, а также исследует возможности внедрения Go для повышения производительности различных дистрибутивов.
Книга ориентирована на программистов и системных администраторов, работающих с Linux, будет интересна разработчикам ядра Linux и драйверов устройств."
            }
        },
    };

    private static Dictionary<string, BookViewModel> _teenagerBooksByFileName = new Dictionary<string, BookViewModel>()
    {
        {
            "BigHero6", new BookViewModel()
            {
                Producer = ProducerType.Ecsmo,
                Name = "Город героев",
                Author = "УЭНО ХАРУКИ",
                AgeLimit = 12,
                PublishingDate = 2023,
                PagesCount = 192,
                Genre = GenreType.BooksForTeenager,
                Price = 638,
                Description = @"Descr"
            }
        },

        {
            "RobinsonCrusoe", new BookViewModel()
            {
                Producer = ProducerType.Ecsmo,
                Name = "Робинзон Крузо: графический роман",
                Author = "ДЕФО ДАНИЭЛЬ",
                AgeLimit = 12,
                PublishingDate = 2018,
                PagesCount = 64,
                Genre = GenreType.BooksForTeenager,
                Price = 205,
                Description = @"Descr"
            }
        },

        {
            "TheCaseOfTheMissingMarquess", new BookViewModel()
            {
                Producer = ProducerType.Ecsmo,
                Name = "Энола Холмс и маркиз в мышеловке",
                Author = "СПРИНГЕР НЭНСИ",
                AgeLimit = 12,
                PublishingDate = 2022,
                PagesCount = 256,
                Genre = GenreType.BooksForTeenager,
                Price = 340,
                Description = @"Descr"
            }
        },
    };

    private static Dictionary<string, BookViewModel> _childBooksByFileName = new Dictionary<string, BookViewModel>()
    {
        {
            "Howl'sMovingCastle", new BookViewModel()
            {
                Producer = ProducerType.Azbuka,
                Name = "Ходячий замок",
                Author = "ДЖОНС ДИАНА УИНН",
                AgeLimit = 12,
                PublishingDate = 2022,
                PagesCount = 448,
                Genre = GenreType.ChildBooks,
                Price = 1075,
                Description = @"Descr"
            }
        },

        {
            "The Little Prince", new BookViewModel()
            {
                Producer = ProducerType.Ecsmo,
                Name = "Маленький принц",
                Author = "ДЕ СЕНТ-ЭКЗЮПЕРИ АНТУАН",
                AgeLimit = 6,
                PublishingDate = 2022,
                PagesCount = 112,
                Genre = GenreType.ChildBooks,
                Price = 1075,
                Description = @"Descr"
            }
        },

        {
            "TheHistoryOfRussiaInStoriesForChildren", new BookViewModel()
            {
                Producer = ProducerType.Ast,
                Name = "История России в рассказах для детей",
                Author = "ДЖОНС ДИАНА УИНН",
                AgeLimit = 6,
                PublishingDate = 2022,
                PagesCount = 320,
                Genre = GenreType.ChildBooks,
                Price = 2006,
                Description = @"Descr"
            }
        },
    };

    private static Dictionary<string, BookViewModel> _comicsBooksByFileName = new Dictionary<string, BookViewModel>()
    {
        {
            "MajorGromHeroForever", new BookViewModel()
            {
                Producer = ProducerType.Bubble,
                Name = "Майор Гром. Герой навсегда",
                Author = "ВОЛКОВ АЛЕКСЕЙ",
                AgeLimit = 16,
                PublishingDate = 2020,
                PagesCount = 112,
                Genre = GenreType.Comics,
                Price = 450,
                Description = @"Descr"
            }
        },
        {
            "Spider-ManBigTime", new BookViewModel()
            {
                Producer = ProducerType.Ecsmo,
                Name = "Человек-паук. Время славы. Том 1",
                Author = "СЛОТТ ДЭН",
                AgeLimit = 16,
                PublishingDate = 2022,
                PagesCount = 520,
                Genre = GenreType.Comics,
                Price = 450,
                Description = @"Descr"
            }
        },
        {
            "StarWarsAgeOfRebellion", new BookViewModel()
            {
                Producer = ProducerType.Comilfo,
                Name = "Звездные войны. Эпоха Восстания. Спецвыпуск",
                Author = "ГУГГЕНХАЙМ МАРК",
                AgeLimit = 16,
                PublishingDate = 2023,
                PagesCount = 36,
                Genre = GenreType.Comics,
                Price = 250,
                Description = @"Descr"
            }
        }
    };

    private static Dictionary<string, BookViewModel> _educationBooksByFileName = new Dictionary<string, BookViewModel>()
    {
        {
            "Mathematics1stClassWorkbook", new BookViewModel()
            {
                Producer = ProducerType.Prosveshenie,
                Name = "Математика. 1 класс. Рабочая тетрадь.",
                Author = "ВОЛКОВА СВЕТЛАНА ИВАНОВНА",
                AgeLimit = 6,
                PublishingDate = 2023,
                PagesCount = 48,
                Genre = GenreType.Education,
                Price = 300,
                Description = @"Descr"
            }
        },

        {
            "MyFirstPrescriptions", new BookViewModel()
            {
                Producer = ProducerType.Litur,
                Name = "Мои первые прописи",
                Author = "БОРТНИКОВА ЕЛЕНА ФЕДОРОВНА",
                AgeLimit = 16,
                PublishingDate = 2008,
                PagesCount = 32,
                Genre = GenreType.Education,
                Price = 150,
                Description = @"Descr"
            }
        },

        {
            "USE2024ComputerScience", new BookViewModel()
            {
                Producer = ProducerType.Exam,
                Name = "ЕГЭ 2024. Информатика.",
                Author = "ЛЕЩИНЕР ВЯЧЕСЛАВ РОАЛЬДОВИЧ",
                AgeLimit = 0,
                PublishingDate = 2024,
                PagesCount = 183,
                Genre = GenreType.Education,
                Price = 450,
                Description = @"Descr"
            }
        },
    };

    private static Dictionary<string, BookViewModel> _fantasyBooksByFileName = new Dictionary<string, BookViewModel>()
    {
        {
            "Dune", new BookViewModel()
            {
                Producer = ProducerType.Ast,
                Name = "Дюна",
                Author = "ГЕРБЕРТ ФРЭНК",
                AgeLimit = 16,
                PublishingDate = 2023,
                PagesCount = 640,
                Genre = GenreType.Fantasy,
                Price = 2450,
                Description = @"Descr"
            }
        },
        {
            "HeavenOfficial'sBlessing", new BookViewModel()
            {
                Producer = ProducerType.Comilfo,
                Name = "Благословение небожителей.",
                Author = "МОСЯН ТУНСЮ",
                AgeLimit = 16,
                PublishingDate = 2022,
                PagesCount = 368,
                Genre = GenreType.Fantasy,
                Price = 1035,
                Description = @"Descr"
            }
        },
        {
            "Twilight", new BookViewModel()
            {
                Producer = ProducerType.Ast,
                Name = "Сумерки",
                Author = "МАЙЕР СТЕФАНИ",
                AgeLimit = 16,
                PublishingDate = 2022,
                PagesCount = 416,
                Genre = GenreType.Fantasy,
                Price = 2000,
                Description = @"Descr"
            }
        }
    };
    
    private static Dictionary<string, BookViewModel> _medBooksByFileName = new Dictionary<string, BookViewModel>()
    {
        {
            "BabyMassage", new BookViewModel()
            {
                Producer = ProducerType.KoronaVek,
                Name = "Детский массаж",
                Author = "КРАСИКОВА ИРИНА СЕМЕНОВНА",
                AgeLimit = 0,
                PublishingDate = 2010,
                PagesCount = 310,
                Genre = GenreType.Medicine,
                Price = 340,
                Description = @"Descr"
            }
        },
        {
            "BloodPathophysiology", new BookViewModel()
            {
                Producer = ProducerType.Patophysiology,
                Name = "Патофизиология крови",
                Author = "ШИФФМАН ФРЕД ДЖ.",
                AgeLimit = 0,
                PublishingDate = 2019,
                PagesCount = 448,
                Genre = GenreType.Medicine,
                Price = 3000,
                Description = @"Descr"
            }
        },
        {
            "Yoga", new BookViewModel()
            {
                Producer = ProducerType.Skifia,
                Name = "Йога. Практика и эффекты асан",
                Author = "ВАЛИКРИ",
                AgeLimit = 12,
                PublishingDate = 2022,
                PagesCount = 368,
                Genre = GenreType.Medicine,
                Price = 480,
                Description = @"Descr"
            }
        }
    };

    private static Dictionary<string, BookViewModel> _scienceBooksByFileName = new Dictionary<string, BookViewModel>()
    {
        {
            "DevelopmentOfSoftwareRequirements", new BookViewModel()
            {
                Producer = ProducerType.Bhv,
                Name = "Разработка требований к программному обеспечению",
                Author = "БИТТИ ДЖОЙ, ВИГЕРС КАРЛ",
                AgeLimit = 0,
                PublishingDate = 2019,
                PagesCount = 736,
                Genre = GenreType.Science,
                Price = 3450,
                Description = @"Descr"
            }
        },
        {
            "InventedInTheUSSR", new BookViewModel()
            {
                Producer = ProducerType.AlpinaNonFiction,
                Name = "Изобретено в СССР",
                Author = "СКОРЕНКО ТИМ",
                AgeLimit = 12,
                PublishingDate = 2019,
                PagesCount = 520,
                Genre = GenreType.Science,
                Price = 2400,
                Description = @"Descr"
            }
        },
        {
            "OrganicChemistry", new BookViewModel()
            {
                Producer = ProducerType.RussianWord,
                Name = "Органическая химия.",
                Author = "ВАЛИКРИ",
                AgeLimit = 0,
                PublishingDate = 2022,
                PagesCount = 384,
                Genre = GenreType.Science,
                Price = 500,
                Description = @"Descr"
            }
        }
    };

    private List<BookViewModel> GetBook(Dictionary<string, BookViewModel> collection, List<ImageFileInfo> imageInfos)
    {
        var books = new List<BookViewModel>();

        foreach (var imageInfo in imageInfos)
        {
            var filename = Path.GetFileNameWithoutExtension(imageInfo.Path);
            if (!collection.ContainsKey(filename))
            {
                Console.WriteLine($"No book image for imageinfo.name={filename}");
                continue;
            }

            var bookViewModel = collection[filename];

            var dir = imageInfo.Genre.ToString();
            var imageViewModel = new ImageViewModel()
            {
                Name = filename,
                Type = Path.GetExtension(imageInfo.Path).ToImageType(),
                Size = imageInfo.Image.Length,
                Base64 = Convert.ToBase64String(imageInfo.Image)
            };

            bookViewModel.Image = imageViewModel;

            books.Add(bookViewModel);
        }

        return books;
    }
    
    public async Task Create(List<ImageFileInfo> imageInfos)
    {
        var progBooks = GetBook(_progBooksByFileName, imageInfos.Where(i => i.Genre == GenreType.ComputerAndProgramming).ToList());
        var teenagerBooks = GetBook(_teenagerBooksByFileName, imageInfos.Where(i => i.Genre == GenreType.BooksForTeenager).ToList());
        var childBooks = GetBook(_childBooksByFileName, imageInfos.Where(i => i.Genre == GenreType.ChildBooks).ToList());
        var comicsBooks = GetBook(_comicsBooksByFileName, imageInfos.Where(i => i.Genre == GenreType.Comics).ToList());
        var educationBooks = GetBook(_educationBooksByFileName, imageInfos.Where(i => i.Genre == GenreType.Education).ToList());
        var fantasyBooks = GetBook(_fantasyBooksByFileName, imageInfos.Where(i => i.Genre == GenreType.Fantasy).ToList());
        var medBooks = GetBook(_medBooksByFileName, imageInfos.Where(i => i.Genre == GenreType.Medicine).ToList());
        var scienceBooks = GetBook(_scienceBooksByFileName, imageInfos.Where(i => i.Genre == GenreType.Science).ToList());
        
        var finalCollection = new List<BookViewModel>();
        finalCollection.AddRange(progBooks);
        finalCollection.AddRange(teenagerBooks);
        finalCollection.AddRange(childBooks);
        finalCollection.AddRange(comicsBooks);
        finalCollection.AddRange(educationBooks);
        finalCollection.AddRange(fantasyBooks);
        finalCollection.AddRange(medBooks);
        finalCollection.AddRange(scienceBooks);

        for (int i = 0; i < finalCollection.Count; ++i)
        {
            for (int j = i + 1; j < finalCollection.Count; ++j)
            {
                var a = finalCollection[i];
                var b = finalCollection[j];
                
                if (a.Id == b.Id)
                {
                    Console.WriteLine($"{a.Name}.{a.Id} == {b.Name}.{b.Id}");
                }
            }
        }

        await _booksCollection.InsertManyAsync(finalCollection);
    }

    public async Task<List<BookViewModel>> GetAllBooksModelsAsync()
    {
        var books = await _booksCollection.Find(new BsonDocument()).ToListAsync();
        return books;
    }

    public async Task AddBookAsync(BookViewModel bookModel)
    {
        await _booksCollection.InsertOneAsync(bookModel);
    }

    public async Task RemoveBookByIdAsync(string id)
    {
        var doc = new BsonDocument("_id", id);
        await _booksCollection.DeleteOneAsync(doc);
    }

    public async Task UpdateBookAsync(string id, BookViewModel newModel)
    {
        await _booksCollection.ReplaceOneAsync(new BsonDocument() { { "_id", id } }, newModel);
    }

    public async Task<List<BookViewModel>> Find(BooksFilter bookFilter)
    {
        var builder = Builders<BookViewModel>.Filter;
        // определяем фильтр - находим все документы, где Name = "Tom"
        //FilterDefinition<BsonDocument> filter = new BsonDocumentFilterDefinition<BsonDocument>();
        //if (request.Name != null)
        FilterDefinition<BookViewModel>? filter = null;

        if (bookFilter.IsEmpty())
        {
            var res = await _booksCollection.FindAsync(builder.Where(d => d.Id != null));
            return res.ToList();
        }

        filter = builder.Where(d => d.Id != null);

        if (bookFilter.Id != null)
        {
            filter &= builder.Where(d => d.Id.Equals(bookFilter.Id.Field));
        }

        if (bookFilter.Producer != null)
        {
            var type = bookFilter.Producer.Field.FromName();
            filter &= builder.Where(d => d.Producer == type);
        }

        if (bookFilter.Author != null)
        {
            filter &= builder.Where(d => d.Author == bookFilter.Author.Field);
        }

        if (bookFilter.AgeLimit != null)
        {
            filter &= builder.Where(d => d.AgeLimit.Equals(bookFilter.AgeLimit.Field));
        }

        if (bookFilter.PublishingDate != null)
        {
            filter &= builder.Where(d => d.PublishingDate != null);

            if (bookFilter.PublishingDate.Type == CompareFieldType.Full)
            {
                filter &= builder.Where(d => d.PublishingDate == bookFilter.PublishingDate.Field);
            }
            else if (bookFilter.PublishingDate.Type == CompareFieldType.Greater)
            {
                filter &= builder.Where(d => d.PublishingDate > bookFilter.PublishingDate.Field);
            }
            else if (bookFilter.PublishingDate.Type == CompareFieldType.Less)
            {
                filter &= builder.Where(d => d.PublishingDate < bookFilter.PublishingDate.Field);
            }
        }

        if (bookFilter.Genre != null)
        {
            filter &= builder.Where(d => d.Genre == bookFilter.Genre.Field);
        }

        if (bookFilter.Name != null)
        {
            filter &= builder.Where(d => d.Name != null);

            if (bookFilter.Name.Type == CompareFieldType.Full)
            {
                filter &= builder.Where(d => d.Name.Equals(bookFilter.Name.Field));
            }
            else if (bookFilter.Name.Type == CompareFieldType.Contains)
            {
                filter &= builder.Where(d => d.Name.Contains(bookFilter.Name.Field));
            }
            else if (bookFilter.Name.Type == CompareFieldType.StartsWith)
            {
                filter &= builder.Where(d => d.Name.StartsWith(bookFilter.Name.Field));
            }
            else if (bookFilter.Name.Type == CompareFieldType.EndsWith)
            {
                filter &= builder.Where(d => d.Name.EndsWith(bookFilter.Name.Field));
            }
        }

        var result = await _booksCollection.FindAsync(filter);
        List<BookViewModel> resultList = result.ToList();
        return resultList;
    }
}