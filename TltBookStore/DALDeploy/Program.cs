﻿// See https://aka.ms/new-console-template for more information

using System.Net.Mime;
using DAL;
using DAL.Factory;
using DAL.Repository;
using Types.Types;

namespace DALDeploy // Note: actual namespace depends on the project name.
{
    internal class EntryPoint
    {
        private static List<ImageFileInfo> GetImagePaths()
        {
            var imagePaths = new List<ImageFileInfo>();

            var enumValues = Enum.GetValues(typeof(GenreType)).Cast<GenreType>();
            string currentDir = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName;
            var imageDir = "Images";
            var dirs = Directory.GetDirectories(Path.Combine(currentDir, imageDir));

            foreach (var dir in dirs)
            {
                var dirName = Path.GetFileNameWithoutExtension(dir);
                GenreType selectedGenreType = enumValues.FirstOrDefault(e => e.ToString() == dirName);
                if (selectedGenreType == GenreType.Default)
                {
                    Console.WriteLine($"Error genretype for dir={dir}");
                }

                var files = Directory.GetFiles(dir);
                foreach (var file in files)
                {
                    if (Path.GetExtension(file) != ".jpg")
                        continue;
                    var absolutePath = Path.Combine(currentDir, imageDir, dir, file);
                    imagePaths.Add(new ImageFileInfo()
                    {
                        Path = file,
                        Genre = selectedGenreType,
                        Image = File.ReadAllBytes(file)
                    });
                }
            }

            return imagePaths;
        }

        static async Task Main(string[] args)
        {
            //var readedLine = Console.ReadLine();
            //if (readedLine == "yes" || readedLine == "y")
            {
                var imagePaths = GetImagePaths();
                await MongoDbKeeper.Deploy(imagePaths);
            }

            // var rep = MongoDbKeeper.GetRepository<ImageRepository>("Images");
            // var all = await rep.FindAll();
            // var f = all[0];
            // File.WriteAllBytes("some.jpg", f.Binary);
        }
    }
}