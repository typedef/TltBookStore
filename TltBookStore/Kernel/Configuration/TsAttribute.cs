using Reinforced.Typings.Attributes;

namespace Kernel.Configuration;

public sealed class TsAttribute : TsClassAttribute
{
    public TsAttribute()
    {
        AutoExportProperties = true;
        AutoExportMethods = true;
        IncludeNamespace = false;
        AutoExportConstructors = false;
    }
}