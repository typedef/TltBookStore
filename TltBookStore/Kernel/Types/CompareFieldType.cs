using Kernel.Configuration;
using Reinforced.Typings.Attributes;

namespace Types.Types;

[TsEnum]
public enum CompareFieldType
{
    Full = 0,
    Contains,
    StartsWith,
    EndsWith,
    Greater,
    Less
}