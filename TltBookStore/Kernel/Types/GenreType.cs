using Reinforced.Typings.Attributes;

namespace Types.Types;

[TsEnum]
public enum GenreType
{
    Default = 0,
    
    Unknown,
    
    // Фантастика
    Fantasy, 
    
    // Научная литература
    Science,     
    
    // Комиксы
    Comics,      
    
    // Манга
    Manga,       
    
    // Детские книги
    ChildBooks,  
    
    // Книги для подростков
    BooksForTeenager, 
    
    // Программирование (Уже сделано)
    ComputerAndProgramming, 
    
    // Образование
    Education,              
    
    // Медицина и здоровье
    Medicine,      
}

public static class GenreTypeHelper
{
    public static string ToName(this GenreType type)
    {
        switch (type)
        {
            case GenreType.Unknown: return "Не выбрано";
            case GenreType.Fantasy: return "Фантастика";
            case GenreType.Science: return "Научная литература";
            case GenreType.Comics: return "Комиксы";
            case GenreType.Manga: return "Манга";
            case GenreType.ChildBooks: return "Детские книги";
            case GenreType.BooksForTeenager: return "Книги для подростков";
            case GenreType.ComputerAndProgramming: return "Программирование";
            case GenreType.Education: return "Образование";
            case GenreType.Medicine: return "Медицина и здоровье";

            default: return "Неизвестно";
        }
    }
}