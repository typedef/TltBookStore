using Reinforced.Typings.Attributes;

namespace Types.Types;

[TsEnum]
public enum ProducerType
{
    Default = 0,
    Unknown,
    
    Piter,
    Bombora,
    Bhv,
    Ecsmo,
    BhvSpb,
    
    // New
    Azbuka,
    Ast,
    Bubble,
    Comilfo,
    Prosveshenie,
    Litur,
    Exam,
    KoronaVek,
    Patophysiology,
    Skifia,
    AlpinaNonFiction,
    RussianWord,
}

public static class ProducerTypeHelper
{
    public static string ToName(this ProducerType producerType)
    {
        switch (producerType)
        {
            case ProducerType.Default: return "По умолчанию";
            case ProducerType.Unknown: return "Не выбран";
            
            case ProducerType.Piter: return "Питер";
            case ProducerType.Bombora: return "Бомбора";
            case ProducerType.Bhv: return "Бхв";
            case ProducerType.Ecsmo: return "Эксмо";
            case ProducerType.BhvSpb: return "BHV-CПБ";
            
            case ProducerType.Azbuka: return "Азбука";
            case ProducerType.Ast: return "АСТ";
            case ProducerType.Bubble: return "Bubble";
            case ProducerType.Comilfo: return "КОМИЛЬФО";
            case ProducerType.Prosveshenie: return "Просвещение";
            case ProducerType.Litur: return "Литур";
            case ProducerType.Exam: return "Экзамен";
            case ProducerType.KoronaVek: return "Корона-Век";
            case ProducerType.Patophysiology: return "Патофизиология";
            case ProducerType.Skifia: return "Скифия";
            case ProducerType.AlpinaNonFiction: return "Альпина нон-фикшн";
            case ProducerType.RussianWord: return "Русское слово";
            
            default: return "Bug";
        }
    }
    
    public static ProducerType FromName(this string producerType)
    {
        switch (producerType)
        {
            case "По умолчанию": return ProducerType.Default;
            case "Не выбран": return ProducerType.Unknown;
            
            case "Питер": return ProducerType.Piter;
            case "Бомбора": return ProducerType.Bombora;
            case "Бхв": return ProducerType.Bhv;
            case "Эксмо": return ProducerType.Ecsmo;
            case "BHV-CПБ": return ProducerType.BhvSpb;
            
            case "Азбука": return ProducerType.Azbuka;
            case "АСТ": return ProducerType.Ast;
            case "Bubble": return ProducerType.Bubble;
            case "КОМИЛЬФО": return ProducerType.Comilfo;
            case "Просвещение": return ProducerType.Prosveshenie;
            case "Литур": return ProducerType.Litur;
            case "Экзамен": return ProducerType.Exam;
            case "Корона-Век": return ProducerType.KoronaVek;
            case "Патофизиология": return ProducerType.Patophysiology;
            case "Скифия": return ProducerType.Skifia;
            case "Альпина нон-фикшн": return ProducerType.AlpinaNonFiction;
            case "Русское слово": return ProducerType.RussianWord;
            
            default: return ProducerType.Default;
        }
    }
}