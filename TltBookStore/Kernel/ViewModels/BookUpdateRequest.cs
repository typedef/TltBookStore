using Kernel.Configuration;

namespace Types.ViewModels;

[Ts]
public class BookUpdateRequest
{
    public string Id { get; set; }
    public BookViewModel NewModel { get; set; }
}