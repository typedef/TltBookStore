using Kernel.Configuration;
using Types.Types;

namespace Types.ViewModels;

[Ts]
public class BookViewModel
{
    public string? Id { get; set; }
    public ProducerType Producer { get; set; }
    public string? Name { get; set; }
    public string? Author { get; set; }
    public int AgeLimit { get; set; }
    public int PublishingDate { get; set; }
    public ImageViewModel Image { get; set; }
    public int PagesCount { get; set; }
    public GenreType Genre { get; set; }
    public uint Price { get; set; }
    public string Description { get; set; }

    public BookViewModel()
    {
        Id = Guid.NewGuid().ToString("N");
    }
}