using Kernel.Configuration;
using Reinforced.Typings.Attributes;
using Types.Types;

namespace Types.ViewModels;

[Ts(AutoExportMethods = false)]
public class BooksFilter
{
    public RequestField<string>? Id { get; set; }
    public RequestField<string>? Producer { get; set; }
    public RequestField<string>? Author { get; set; }
    public RequestField<int>? AgeLimit { get; set; }
    public RequestField<string>? Name { get; set; }
    public RequestField<int>? PublishingDate { get; set; }
    public RequestField<GenreType>? Genre { get; set; }

    public bool IsEmpty()
    {
        if (Id == null && Producer == null && Author == null 
            && AgeLimit == null && Name == null && PublishingDate == null 
            && Genre == null)
            return true;
        return false;
    }
}