using Kernel.Configuration;
using Types.Types;

namespace Types.ViewModels;

[Ts]
public class EnumValue<T>
{
    public string Text { get; set; }
    public T Value { get; set; }
}

[Ts]
public class BooksFilterValuesViewModel
{
    public List<string> Names { get; set; }
    public List<EnumValue<GenreType>> Genres { get; set; }
    public List<string> Producers { get; set; }
    public List<string> Authors { get; set; }
    public List<int> PublishingDates { get; set; }
    // public int AgeLimit { get; set; }
    // public int Price { get; set; }
    
    public BooksFilterValuesViewModel()
    {
        Names = new List<string>();
        Genres = new List<EnumValue<GenreType>>();
        Producers = new List<string>();
        Authors = new List<string>();
        PublishingDates = new List<int>();
    }
}