using Kernel.Configuration;
using Reinforced.Typings.Attributes;

namespace Types.ViewModels;

[TsEnum]
public enum ImageType
{
    Default = 0,
    
    Jpg,
}

public static class ImageTypeHelper
{
    public static string ToName(this ImageType imageType)
    {
        return $".{imageType.ToString().ToLower()}";
    }
    
    public static ImageType ToImageType(this string imageType)
    {
        switch (imageType)
        {
            case ".jpg": return ImageType.Jpg;
        
            default: return ImageType.Default;
        }
    }
}

[Ts]
public class ImageViewModel
{
    public string Name { get; set; }
    public ImageType Type { get; set; }
    public object Object { get; set; }
    public long Size { get; set; }
    public string Base64 { get; set; }
}