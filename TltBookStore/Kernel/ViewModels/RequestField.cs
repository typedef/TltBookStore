using Kernel.Configuration;
using Types.Types;

namespace Types.ViewModels;

[Ts]
public class RequestField<T>
{
    public T Field { get; set; }
    public CompareFieldType Type { get; set; }

    public RequestField(T field, CompareFieldType type)
    {
        Field = field;
        Type = type;
    }
}