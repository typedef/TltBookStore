using Types.ViewModels;

namespace Services.Contracts;

public interface IBookService
{
    Task<List<BookViewModel>> GetAllBooksAsync();

    Task AddBookAsync(BookViewModel bookModel);

    Task RemoveBookByIdAsync(string id);

    Task UpdateBookAsync(string id, BookViewModel newModel);

    Task<List<BookViewModel>> Find(BooksFilter bookFilter);
}