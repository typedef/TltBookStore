using System.Diagnostics.CodeAnalysis;
using DAL.Factory;
using DAL.Repository;
using Services.Contracts;
using Types.ViewModels;

namespace Services.Services;

[SuppressMessage("ReSharper", "ConvertConstructorToMemberInitializers")]
public class BookService : IBookService
{
    private readonly BookRepository _repository;
    
    public BookService()
    {
        _repository = MongoDbKeeper.GetRepository<BookRepository>("Books");
    }
    
    public async Task<List<BookViewModel>> GetAllBooksAsync()
    {
        var allModels = await _repository.GetAllBooksModelsAsync();
        return allModels;
    }

    public async Task AddBookAsync(BookViewModel bookModel)
    {
        await _repository.AddBookAsync(bookModel);
    }
    
    public async Task RemoveBookByIdAsync(string id)
    {
        await _repository.RemoveBookByIdAsync(id);
    }
    
    public async Task UpdateBookAsync(string id, BookViewModel newModel)
    {
        await _repository.UpdateBookAsync(id, newModel);
    }

    public async Task<List<BookViewModel>> Find(BooksFilter bookFilter)
    {
        return await _repository.Find(bookFilter);
    }
}