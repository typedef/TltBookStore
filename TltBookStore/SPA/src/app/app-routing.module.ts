import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {BooksComponent} from "./components/books/books.component";
import {PageNotFoundComponent} from "./components/page-not-found/page-not-found.component";
import {PayComponent} from "./components/pay/pay.component";
import {AuthorsComponent} from "./components/authors/authors.component";
import {MainComponent} from "./components/main/main.component";

const routes: Routes = [
  {path: 'main', component: MainComponent},
  {path: 'books', component: BooksComponent},
  {path: 'pay', component: PayComponent},
  {path: 'author', component: AuthorsComponent},
  {path: '**', component: PageNotFoundComponent}
];

// configures NgModule imports and exports
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
