import {Component} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public isLoggedIn: boolean = false;

  constructor(private _router: Router) {

  }

  public navigateToMain(): void {
    this._router.navigate(["main"]);
  }
  public navigateToBooks(): void {
    this._router.navigate(["books"]);
  }
  public navigateToAuthor(): void {
    this._router.navigate(["author"]);
  }
  public navigateToPay(): void {
    this._router.navigate(["pay"]);
  }

  public onLoginClicked(): void {
    console.log("Not doing much on login clicked for now!");

  }

  public onRegisterClicked(): void {
    console.log("Not doing much on register clicked for now!");
  }

}
