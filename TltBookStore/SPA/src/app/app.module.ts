import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import {AppComponent} from './app.component';
import {AppRoutingModule} from "./app-routing.module";

// Ng Material's
import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatSelectModule} from '@angular/material/select';
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';

// Services
import {BooksService} from "./core/services/books/books.service";
import {BooksComponent} from "./components/books/books.component";
import {BookConcreteComponent} from "./components/book-concrete/book-concrete.component";
import {PageNotFoundComponent} from "./components/page-not-found/page-not-found.component";
import {PayComponent} from "./components/pay/pay.component";
import {CacheService} from "./core/services/app/cache.service";
import { AuthorsComponent } from './components/authors/authors.component';
import { MainComponent } from './components/main/main.component';
//import {DropdownComponent} from './components/dropdown/dropdown.component';


// Components


@NgModule({
  declarations: [
    AppComponent,
    BooksComponent,
    BookConcreteComponent,
    PageNotFoundComponent,
    PayComponent,
    AuthorsComponent,
    MainComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NoopAnimationsModule,
    FormsModule,
    ReactiveFormsModule,


    AppRoutingModule,

    // Ng Material's
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatIconModule,
    MatInputModule,
  ],
  providers: [
    HttpClient,

    BooksService,
    CacheService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
