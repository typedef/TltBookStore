import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {

  public students: Student[] = [];

  constructor() {
    // same info
    let group: string = "ПИб-2106а";

    let miraShi = new Student();
    miraShi.name = "Шидер Мира";
    miraShi.group = group;
    miraShi.description = "Менеджер, аналитик и разработчик";
    miraShi.imgUrl = "url(../../assets/MiraS.jpg)";

    let mamievDmi = new Student();
    mamievDmi.name = "Мамиев Дмитрий";
    mamievDmi.group = group;
    mamievDmi.description = "Аналитик, менеджер и разработчик";
    mamievDmi.imgUrl = "url(../../assets/MamievD.jpg)";

    this.students.push(miraShi);
    this.students.push(mamievDmi);
  }

  ngOnInit(): void {
  }

}

export class Student {
  name: string;
  group: string;
  description: string;
  imgUrl: string;
}
