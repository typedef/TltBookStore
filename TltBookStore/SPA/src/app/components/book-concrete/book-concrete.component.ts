import {Component, Input, OnInit} from '@angular/core';
import {BookViewModel} from "../../core/view-models";

@Component({
  selector: 'book-concrete',
  templateUrl: './book-concrete.component.html',
  styleUrls: ['./book-concrete.component.css']
})
export class BookConcreteComponent implements OnInit {

  @Input() book: BookViewModel;

  constructor() { }

  ngOnInit(): void {
  }

  public getImage(): string {
    return `data:image/jpg;base64, ${this.book.Image.Base64}`;
  }

}
