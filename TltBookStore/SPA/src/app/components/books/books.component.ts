import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {
  BooksFilter,
  BooksFilterValuesViewModel,
  BookViewModel, CompareFieldType,
  GenreType,
  ImageViewModel, ProducerType, RequestField
} from "../../core/view-models";
import {BooksService} from "../../core/services/books/books.service";
import {CacheService} from "../../core/services/app/cache.service";
import {map, Observable, startWith} from "rxjs";
import {FormControl} from "@angular/forms";

@Component({
  selector: 'books-component',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  public allBooks: BookViewModel[] = [];

  public isLoading: boolean = false;

  public isNotLoading(): boolean {
    return !this.isLoading;
  }

  public isBookSelected: boolean = false;
  public selectedBook: BookViewModel;

  // filters
  public bookFilterValues: BooksFilterValuesViewModel;
  public selectedName: string = '';
  public selectedGenre: GenreType = GenreType.Unknown;
  public selectedProducer: string = '';
  public selectedAuthor: string = '';
  public selectedDate: number = 0;

  public filteredNames: Observable<string[]>;

  constructor(private _booksService: BooksService,
              private _router: Router,
              private _cache: CacheService) {
    this.findBooks();
    this.getFilterValues();

  }

  ngOnInit(): void {
  }

  public getImage(ivm: ImageViewModel): string {
    return `data:image/jpg;base64, ${ivm.Base64}`;
  }

  public findBooks(): any {
    let filter = new BooksFilter();

    if (this.selectedName !== '') {
      filter.Name = new RequestField<string>();
      filter.Name.Field = this.selectedName;
      filter.Name.Type = CompareFieldType.Contains;
    }

    if (this.selectedGenre !== GenreType.Unknown) {
      filter.Genre = new RequestField();
      filter.Genre.Field = this.selectedGenre;
      filter.Genre.Type = CompareFieldType.Full;
    }

    if (this.selectedProducer !== '') {
      filter.Producer = new RequestField();
      filter.Producer.Field = this.selectedProducer;
      filter.Producer.Type = CompareFieldType.Full;
    }

    if (this.selectedAuthor != '') {
      filter.Author = new RequestField();
      filter.Author.Field = this.selectedAuthor;
      filter.Author.Type = CompareFieldType.Full;
    }

    if (this.selectedDate != 0) {
      filter.PublishingDate = new RequestField();
      filter.PublishingDate.Field = this.selectedDate;
      filter.PublishingDate.Type = CompareFieldType.Full;
    }

    this.isLoading = true;
    let sub = {
      next: (success: BookViewModel[]): void => {
        this.allBooks = success;
        this.isLoading = false;
      },
      error: (err: any): void => {
        console.error(err);
        this.isLoading = false;
      }
    };

    this._booksService.findBooks(filter).subscribe(sub);
  }

  public getFilterValues(): any {
    this.isLoading = true;
    let sub = {
      next: (success: BooksFilterValuesViewModel): void => {
        this.bookFilterValues = success;
        this.isLoading = false;
      },
      error: (err: any): void => {
        console.error(err);
        this.isLoading = false;
      }
    };

    this._booksService.getFilterValues().subscribe(sub);
  }

  public applyFilter(): void {
    this.findBooks();
  }

  public removeFilters(): void {
    this.selectedName = '';
    this.selectedGenre = GenreType.Unknown;
    this.selectedProducer = '';
    this.selectedAuthor = '';
    this.selectedDate = 0;
    this.findBooks();
  }

  public buyBook(book: BookViewModel): void {
    this._cache.setPayBook(book);
  }

  public goToConcreteBook(book: BookViewModel): void {
    this.isBookSelected = true;
    this.selectedBook = book;
  }

  public goBack(): void {
    this.isBookSelected = false;
  }

}
