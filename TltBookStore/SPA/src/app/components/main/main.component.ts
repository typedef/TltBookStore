import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  public header: string = "TLT Book Store";
  public description: string = "- проект призванный автоматизировать книжный учёт и привлечь новых клиентов.";

  constructor() { }

  ngOnInit(): void {
  }

}
