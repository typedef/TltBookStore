import {Component, OnInit} from '@angular/core';
import {CacheService} from "../../core/services/app/cache.service";
import {BookViewModel, ImageViewModel} from "../../core/view-models";

@Component({
  selector: 'app-pay',
  templateUrl: './pay.component.html',
  styleUrls: ['./pay.component.css']
})
export class PayComponent implements OnInit {

  public payBooks: BookViewModel[] = [];

  constructor(private _cache: CacheService) {
    this.reinitBooks();
  }

  private reinitBooks(): void {
    this.payBooks = this._cache.getPayBook();
  }

  ngOnInit(): void {
  }

  public getImage(ivm: ImageViewModel): string {
    return `data:image/jpg;base64, ${ivm.Base64}`;
  }

  public deleteBook(book: BookViewModel): void {
    this._cache.removePayBook(book);
    this.reinitBooks();
  }

  public getBooksCount(): number {
    return this._cache.getPayBook().length;
  }

  public getBooksPrice(): number {
    let sum: number = 0;
    this._cache.getPayBook().forEach(b => sum += b.Price);
    return sum;
  }

  protected readonly alert = alert;
}
