import {Injectable} from "@angular/core";
import {BookViewModel} from "../../view-models";
import {BooksComponent} from "../../../components/books/books.component";

@Injectable()
export class CacheService {

  // const's
  public PAY_BOOK: string = "payBooks";

  public getPayBook(): BookViewModel[] {
    let books: BookViewModel[] = [];
    let bookItem: string | null = localStorage.getItem(this.PAY_BOOK);

    if (bookItem !== null) {
      books = JSON.parse(bookItem);
    }

    return books;
  }

  public setPayBook(book: BookViewModel): void {
    let books = this.getPayBook();
    books.push(book);
    let bookItem = JSON.stringify(books);
    localStorage.setItem(this.PAY_BOOK, bookItem);
  }

  private arrRemove(arr: BookViewModel[], item: BookViewModel): BookViewModel[] {
    let ind = -1;

    for (let i = 0; i < arr.length; ++i) {
      let b = arr[i];
      if (item.Id === b.Id) {
        ind = i;
        break;
      }
    }

    if (ind !== -1) {
      arr.splice(ind, 1);
    }

    return arr;
  }

  public removePayBook(book: BookViewModel): void {
    let books = this.getPayBook();
    books = this.arrRemove(books, book);
    let bookItem = JSON.stringify(books);
    localStorage.setItem(this.PAY_BOOK, bookItem);
  }

  // reference https://learn.javascript.ru/cookie
  public add(key: string, val: string, expiresInSeconds: number = 2 * 3600 /* 2 hours */): void {
    document.cookie = `${key}:${val}; max-age=${expiresInSeconds}`;
  }

  public get(cacheKey: string): any {
    let cachedVals: string[] = document.cookie.split(';');
    for (let i = 0; i < cachedVals.length; ++i) {
      let pair = cachedVals[i];
      let key = pair.split(":")[0].trim();
      let val = pair.substring(pair.indexOf(":") + 1, pair.length).trim();
      if (key === cacheKey) {
        return val;
      }
    }

    return null;
  }

  public update(cacheKey: string, cacheValue: any, expiresInSeconds: number = 2 * 3600 /* 2 hours */): void {
    document.cookie = `${cacheKey}:${cacheValue}; max-age=${expiresInSeconds}`;
  }
}
