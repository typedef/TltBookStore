import {Injectable} from "@angular/core";
import {BaseService} from "../base-service";
import {Observable} from "rxjs";
import {HttpHeaders} from "@angular/common/http";
import {BooksFilter, BooksFilterValuesViewModel, BookViewModel} from "../../view-models";

@Injectable()
export class BooksService extends BaseService {

  private controller: string = this.hostUrl + "books/";

  public getFilterValues(): Observable<BooksFilterValuesViewModel> {
    return this.httpClient.get(this.controller + "get-filter-values") as Observable<BooksFilterValuesViewModel>;
  }

  public findBooks(filter: BooksFilter = new BooksFilter()): Observable<BookViewModel[]> {
    return this.httpClient.post(this.controller + "find", filter) as Observable<BookViewModel[]>;
  }

}
