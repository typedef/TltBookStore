import { Injectable } from "@angular/core";
import { CanActivate } from "@angular/router";
import { CacheService } from "../app/cache.service";

@Injectable()
export class GuardService implements CanActivate {
  constructor(private _cacheService: CacheService) {
  }

  public canActivate(): boolean {
    debugger;
    let userJson = this._cacheService.get("user");
    let canActivate = ((userJson !== null) && (userJson !== undefined));
    return canActivate;
  }
}
