using Services.Contracts;
using Services.Services;

namespace WebApi.Config;

public class AppConfig
{
    public void Init(IServiceCollection services)
    {
        services.AddTransient<IBookService, BookService>();
    }
}

