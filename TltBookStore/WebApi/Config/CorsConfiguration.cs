namespace WebApi.Config;

public class CorsConfiguration
{
    public const string CorsPolicyName = "BookStoreCorsPolicy";
    public const string CorsOrigin = "http://localhost:4200";
}