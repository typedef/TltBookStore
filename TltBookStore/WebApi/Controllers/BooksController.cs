using System.Collections.Immutable;
using Microsoft.AspNetCore.Mvc;
using Services.Contracts;
using Types.Types;
using Types.ViewModels;

namespace WebApi.Controllers;

[ApiController]
[Route("api/books")]
public class BooksController
{
    private readonly IBookService _bookService;

    public BooksController(IBookService bookService)
    {
        _bookService = bookService;
    }

    [HttpGet]
    [Route("get-all")]
    public async Task<List<BookViewModel>> GetAllBooks()
    {
        List<BookViewModel> books = await _bookService.GetAllBooksAsync();
        return books;
    }

    [HttpPost]
    [Route("add")]
    public async Task AddBook(BookViewModel book)
    {
        await _bookService.AddBookAsync(book);
    }

    [HttpPost]
    [Route("remove-by-id")]
    public async Task RemoveBookByIdAsync(string id)
    {
        await _bookService.RemoveBookByIdAsync(id);
    }

    [HttpPost]
    [Route("update")]
    public async Task UpdateBook(BookUpdateRequest request)
    {
        await _bookService.UpdateBookAsync(request.Id, request.NewModel);
    }

    private delegate string ToName<T>(T t);
    private delegate bool EnumFilter<T>(T t);
    
    private List<EnumValue<T>> GetEnumValues<T>(ToName<T> toName, EnumFilter<T> filter) where T: struct, IConvertible
    {
        Type enumType = typeof(T);
        if (!enumType.IsEnum)
        {
            throw new ArgumentException("T should be enum type!");
        }
        
        var list = new List<EnumValue<T>>();

        var values = Enum.GetValues(enumType).Cast<T>();
        foreach (var val in values)
        {
            if (!filter(val))
                continue;
            
            var name = toName(val);
            list.Add(new EnumValue<T>()
            {
                Text = name,
                Value = val,
            });
        }
        
        return list;
    }
    
    [HttpGet]
    [Route("get-filter-values")]
    public async Task<BooksFilterValuesViewModel> GetFilterValuesAsync()
    {
        var bookFilterValues = new BooksFilterValuesViewModel();

        bookFilterValues.Genres = GetEnumValues<GenreType>(t => t.ToName(), t => t != GenreType.Default);
        bookFilterValues.Producers = GetEnumValues<ProducerType>(
                t => t.ToName(), 
                t => t != ProducerType.Default)
            .Select(e => e.Value.ToName())
            .ToList();

        var result = await FindAsync(new BooksFilter());
        bookFilterValues.Names = result.Select(r => new string(r.Name)).Distinct().OrderByDescending(v => v).ToList();
        bookFilterValues.Authors = result.Select(r => new string(r.Author)).Distinct().OrderByDescending(v => v).ToList();
        bookFilterValues.PublishingDates = result.Select(r => r.PublishingDate).Distinct().OrderByDescending(v => v).ToList();
        
        return bookFilterValues;
    }

    [HttpPost]
    [Route("find")]
    public async Task<List<BookViewModel>> FindAsync(BooksFilter bookFilter)
    {
        List<BookViewModel> books = await _bookService.Find(bookFilter);
        return books;
    }
}