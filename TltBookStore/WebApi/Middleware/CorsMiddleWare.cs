using System.Net;
using Microsoft.AspNetCore.Cors.Infrastructure;
using WebApi.Config;

namespace WebApi.Middleware;

public class CorsMiddleWare
{
    private readonly RequestDelegate next;
  
    public CorsMiddleWare(RequestDelegate next)
    {
        this.next = next;
    }
  
    public async Task InvokeAsync(HttpContext context)
    {
        context.Response.Headers.Add("Content-Type", "Content-Type, Authorization, X-Requested-With application/json");
        context.Response.Headers.Add("Access-Control-Allow-Origin", CorsConfiguration.CorsOrigin);
        context.Response.Headers.Add("Access-Control-Allow-Methods", "*");
        context.Response.Headers.Add("Access-Control-Allow-Header", "*");

        if (context.Request.Method == "OPTIONS")
        {
            context.Response.StatusCode = (int)HttpStatusCode.OK;
        }
        
        await next.Invoke(context);
        
    }
}