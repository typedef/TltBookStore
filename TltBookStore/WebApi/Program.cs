using DAL.Factory;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Cors.Infrastructure;
using WebApi.Config;
using WebApi.Middleware;

MongoDbKeeper.Init();

var builder = WebApplication.CreateBuilder(args);

new AppConfig().Init(builder.Services);

// Add services to the container. 
builder.Services.AddControllers()
    .AddJsonOptions(o => 
        o.JsonSerializerOptions.PropertyNamingPolicy = new UpperCamelCasePolicy());

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// // Enable cors
builder.Services.AddCors(corsOptions =>
{
    corsOptions.AddPolicy(CorsConfiguration.CorsPolicyName,
        confPolicy => confPolicy.WithOrigins(CorsConfiguration.CorsOrigin)
            .AllowAnyMethod()
            .AllowAnyHeader()
            .AllowCredentials());
});

// builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
//     .AddCookie(options =>
//     {
//         options.ExpireTimeSpan = TimeSpan.FromMinutes(20);
//         options.SlidingExpiration = true;
//         options.AccessDeniedPath = "/Denied/";
//     });

var app = builder.Build();

// app.UseMiddleware<CorsMiddleWare>();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseCors(CorsConfiguration.CorsPolicyName);

// app.UseAuthorization();
// app.UseAuthentication();
//
// var cookiePolicyOptions = new CookiePolicyOptions
// {
//     MinimumSameSitePolicy = SameSiteMode.Strict,
// };
// app.UseCookiePolicy(cookiePolicyOptions);

app.MapControllers();

app.Run();